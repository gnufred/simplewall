window.addEvent('domready', function(){
	debug = window.location.pathname.match("/dev/");
	timer = {};
	initialize_google_analytics();
	register_tracking_events();
});

function initialize_google_analytics(){
	if(debug) return;
	
	google_analytics = _gat._getTracker('UA-27020116-1');
	google_analytics._trackPageview();
}

function register_tracking_events(){
	$$('#facebook a.add').addEvent('click',function(){trackEvent('Facebook', 'Account', 'Add')});
	$$('#facebook a.remove').addEvent('click',function(){trackEvent('Facebook', 'Account', 'Remove')});
	$$('#twitter a.add').addEvent('click',function(){trackEvent('Twitter', 'Account', 'Add')});
	$$('#twitter a.remove').addEvent('click',function(){trackEvent('Twitter', 'Account', 'Remove')});
	document.addEvent('feedLoading', function(network, time){trackEvent(network, 'Feed', 'Loading ('+time+')', 'Start')});
	document.addEvent('feedLoaded', function(network, time){trackEvent(network, 'Feed', 'Loaded ('+time+')', 'Stop')});
	document.addEvent('wallLoaded', function(network, time){add_wall_tracking_events(time)});
	document.addEvent('error', function(network, error){trackEvent(network, 'Error', error)});
}

function add_wall_tracking_events(time){
	trackEvent('SimpleWall', 'Feed', 'Loaded ('+time+')', 'Stop');
	$$('a.twitter').addEvent('click',function(){trackEvent('SimpleWall','Click','Twitter')});
	$$('a.facebook').addEvent('click',function(){trackEvent('SimpleWall','Click','Facebook')});
	$$('div#more').addEvent('click',function(){trackEvent('SimpleWall', 'Click', 'Load More')});
}

function trackEvent(category, action, label, timer_action){
	if(timer_action == 'Start') {
		timer[category + action] = new TimeTracker();
		timer[category + action]._recordStartTime();
	} else if(timer_action == 'Stop'){
		timer[category + action]._recordEndTime();
		var time = timer[category + action]._getTimeDiff();
	}

	if(debug){
		if(!time) time = '';
		if (typeof console != "undefined")
			console.log(category +' '+ action +' '+ label + ' ' + time);
	}
	else {
		if (time)
			google_analytics._trackEvent(category, action, label, time);
		else
			google_analytics._trackEvent(category, action, label);
	}
}

// Copyright 2007 Google, Inc.
// http://www.apache.org/licenses/LICENSE-2.0 for license details.
/**
 * @param {Array.<Number>} arg1 Optional array that represents the bucket
 */
var TimeTracker = function(opt_bucket) {
	if (opt_bucket)
		this.bucket_ = opt_bucket.sort(this.sortNumber); 
	else
		this.bucket_ = TimeTracker.DEFAULT_BUCKET;
};

TimeTracker.prototype.startTime_;
TimeTracker.prototype.stopTime_;
TimeTracker.prototype.bucket_;
TimeTracker.DEFAULT_BUCKET = [100, 500, 1500, 2500, 5000];

/**
 * Calculates time difference between start and stop
 * @return {Number} The time difference between start and stop
 */
TimeTracker.prototype._getTimeDiff = function() {
	return (this.stopTime_ - this.startTime_);
};

/**
 * Helper function to sort an Array of numbers
 */
TimeTracker.prototype.sortNumber = function(a, b) { return (a - b) }

/**
 * Records the start time
 * @param {Number} arg1 Optional start time specified by user
 */
TimeTracker.prototype._recordStartTime = function(opt_time) {
	if (opt_time != undefined)
		this.startTime_ = opt_time;
	else
		this.startTime_ = (new Date()).getTime();
};

/**
 * Records the stop time
 * @param {Number} arg1 Optional stop time specified by user
 */
TimeTracker.prototype._recordEndTime = function(opt_time) {
	if (opt_time != undefined)
		this.stopTime_ = opt_time;
	else
		this.stopTime_ = (new Date()).getTime();
};