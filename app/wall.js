window.addEvent('domready', function(){ app = new App() });

var App = new Class({
    initialize: function(){
        this.register_events();
        gui = new GUI();
		this.initialize_networks();
		this.show_login_if_no_network();
    },
	
	initialize_networks: function(){
		network = {};
		if(Cookie.read('use_facebook')) network.facebook = new Facebook('285812584763963');
		if(Cookie.read('use_twitter')) network.twitter = new Twitter();
	},

    register_events: function(){
        document.addEvent('feedLoaded', function(network, time){ gui.show_wall(time) });
        document.addEvent('userStatusChange', function(){ this.react_to_user_status_change() }.bind(this));
    },
	
	show_login_if_no_network: function(){
		var nb_networks = 0;
		for(i in network) nb_networks++;
		if(nb_networks < 1) gui.show_login();
		else document.fireEvent('feedLoading', ['SimpleWall', 'current']);
	},
   
    react_to_user_status_change: function(){
		/* Exit if not all network received loggin status */
		for(i in network) if(network[i].is_logged() == null) return;
		
		if(gui.logged_networks() < 1) {
            gui.show_login();
			return;
		}
		
		gui.show_loading();
		
		for(i in network) if(network[i].is_logged()) network[i].feed.load();
	}   
});

var GUI = new Class({
    remove_loading: function(){
        if($('loading')) $('loading').dispose();
    },

    show_login: function(){
        window.location = "login.php";
    },
	
	clear_wall: function(){
		$('wall').set('html', '');
	},
	
	activate_load_newer: function(){
		if(network.facebook) {
			setTimeout(function(){
				network.facebook.refresh_access_token();
			}, 14*60*1000);
		}
		setTimeout(function(){
			document.fireEvent('feedLoading', ['SimpleWall', 'newer']);
			for(i in network) if(network[i].is_logged()) network[i].feed.load('newer');
		}, 15*60*1000);
	},
	
	show_load_older: function(){
		new Element('div', {
			id : 'more',
			'class' : 'feed_entry simplewall',
			events : {
				click : function() {
					this.load_older();
				}.bind(this)
			}
		}).adopt(
			new Element('p', {
				'text' : 'Read more updates'
			})
		).inject($('wall'));
	},
    
    show_loading: function(){
		if($('loading')) return;
		
		new Element('div', {
				id : 'loading',
				'class' : 'feed_entry simplewall'
			}).adopt(
				new Element('p', {
					'class' : 'day',
					'text' : 'Loading'
				})).inject($('wall'));
    },
	
	load_older: function(){
		document.fireEvent('feedLoading', ['SimpleWall', 'older']);
		$('more').dispose();
		this.show_loading();
	
		for(i in network) if(network[i].is_logged())	network[i].feed.load('older');
	},
    
    show_wall: function(time){
		for(i in network) if(network[i].is_logged() && !network[i].feed.is_loaded()) return;
	
		this.clear_wall();
		this.show_date_separators();
		
		if(this.logged_networks() > 1) {
			var feed = new Array();
		
			for(i in network) if(network[i].is_logged()) feed = this.merge_feeds(feed, network[i].feed.entry);
			
			this.show_feed_entries(feed);
		} else
			for(i in network) if(network[i].is_logged()) this.show_feed_entries(network[i].feed.entry);
		
		this.show_load_older();
		this.activate_load_newer();
		
		document.fireEvent('wallLoaded', ['SimpleWall', time]);
    },
	
	logged_networks: function(){
		var nb_networks = 0;
		for(i in network) if(network[i].is_logged()) nb_networks++;
		return nb_networks;
	},
	
	merge_feeds: function(feed, entries){;
			
		for(i in entries) if(entries[i].source) feed.push(entries[i]);
				
		feed.sort(function(a, b){
			return b.story.index - a.story.index;
		});
		
		return feed;
	},	

	show_date_separators: function(){
		this.get_seperator_element('Today').inject($('wall'));
		this.get_seperator_element('Yesterday').inject($('wall'));
		this.get_seperator_element('Later').inject($('wall'));
	},
	
	get_seperator_element: function(day) {
		return new Element('div', {
				id : day.toLowerCase(),
				'class' : 'feed_entry simplewall',
				'styles' : {
					'display' : 'none'
				}
			}).adopt(
				new Element('p', {
					'class' : 'day',
					'text' : day
				}));
	},
	
	show_feed_entries: function(feed){
		if(this.logged_networks() > 1) {
			var entries_to_show = feed.length * 0.5;
			
			Array.each(feed, function(entry, index) {
				if(index < entries_to_show) entry.show();
			});
			
			return;
		}
		
		Array.each(feed, function(entry, index) { entry.show() });
	}
});

var Network = new Class({
    feed: null,
    user_logged_in: null,

    initialize: function(api_key){
        this.load_api(api_key);
        this.get_login_state();
		this.create_feed_object();
    },
    
    is_logged: function(){
        return this.user_logged_in;
    },
	
	get_login_state: function(){
		setTimeout(function(){
			this.update_user_status(true);
		}.bind(this), 0);
    },
	
	update_user_status: function(userStatus) {
        this.user_logged_in = userStatus;
        document.fireEvent('userStatusChange');
    },
	
	load_api: function(){}
});

var Twitter = new Class({
	Extends: Network,
	
	create_feed_object: function(){
		this.feed = new TwitterFeed();
	},
	
	logout: function(){
		Cookie.dispose('use_twitter');
	}
	
});

var Facebook = new Class({
    Extends: Network,
	token: 0,
   
    load_api: function(api_key){
        new Element('div', {id : 'fb-root'}).inject($('body'), 'top');
        FB.init({ appId: api_key, cookie:true, status: false, oauth: true });
    },

	create_feed_object: function(){
		this.feed = new FacebookFeed();
	},
    
    get_login_state: function(){
		FB.getLoginStatus(function(response) {
			if(response.status == 'connected') {
				FB.api('/fql', {'q' : 'SELECT read_stream FROM permissions WHERE uid = me()' }, function(response){
					this.update_user_status(response.data[0].read_stream);
				}.bind(this));
			}
			else
				this.update_user_status(false);
		}.bind(this));
    },
	
	refresh_access_token: function(callback){
		FB.getLoginStatus();
	}
});

var Feed = new Class({
    entry :  new Array(),
	id : { first: 0, last : 0 },
	loaded : false,
	
	is_loaded: function(){
		return this.loaded;
	},
	
	order: function(){
		this.entry.sort(function(a, b){
			return b.story.index - a.story.index;
		});
	}
});

var TwitterFeed = new Class({
	Extends: Feed,
	
	load: function(time){
		if(!time) var time = 'current';
		document.fireEvent('feedLoading', ['Twitter', time]);
		this.loaded = false;
	
		new Request.JSON({
			url : "twitter/ajax/feed.php",
			onSuccess: function(json){
				if(json.response[0]) {
					if(time != 'older') this.id.first = json.response[0].id_str;
					if(time != 'newer') this.id.last = json.response.getLast().id_str;
					//Since twitter send tweet equal to the max_id we get a duplicate and need to remove it
					if(time == 'older') json.response.splice(0, 1);
					this.parse(json.response);
					if(time == 'newer') this.order();
				}
				this.loaded = true;
				document.fireEvent('feedLoaded', ['Twitter', time]);
			}.bind(this),
			onFailure: function(xhr){
				this.loaded = true;
				document.fireEvent('error', ['Twitter', xhr.status+' '+xhr.statusText]);
			}
		}).get(this.get_request_options(time));
	},
	
	get_request_options: function(time){
		if(time == 'older') return {'max_id':this.id.last};
		else if(time == 'newer') return {'since_id':this.id.first};
		else {};
	},
		
	parse: function(response){
		Array.each(response, function(data) {
			if(data.text.match(/^RT /i)) return;
		
			this.entry.push ( new TwitterFeedEntry(data) );
		}.bind(this));
	}
});

var FacebookFeed = new Class({
    Extends: Feed,
	
	load: function(time){
		if(!time) var time = 'current';
		document.fireEvent('feedLoading', ['Facebook', time]);
		this.loaded = false;
				
		var options = this.get_request_options(time);
	
        FB.api('/me/home', options, function(response1){
			if(response1.error) {
				document.fireEvent('error', ['Facebook', response1.error.message]);
				this.loaded = true;
				return;
			}
		
			if(response1.paging && time != 'older') this.id.first = new URI(response1.paging.previous).get('data').since;
			
			if(!response1.paging || time == 'newer'){
				this.parse(response1.data)
				if(time == 'newer') this.order();
				this.loaded = true;
				document.fireEvent('feedLoaded', ['Facebook', time]);
				return;
			}
			
			options.until = new URI(response1.paging.next).get('data').until;
		
			FB.api("/me/home", options , function(response2){
					if(response2.error) document.fireEvent('error', ['Facebook', error]);

					this.parse(response1.data.combine(response2.data));
					if(response2.paging) this.id.last = new URI(response2.paging.next).get('data').until;
					this.loaded = true;
					document.fireEvent('feedLoaded', ['Facebook', time]);
					
				}.bind(this));
        }.bind(this));
    },
	
	get_request_options: function(time){
		var options = { fields : 'id,from,to,message,link,name,actions,type,created_time,application,caption' };
		if(time == 'older') options.until = this.id.last;
		else if(time == 'newer') options.since = this.id.first;
		return options;
	},
	
    parse: function(response){ 
            
        Array.each( response, function(data){
			if(!data.actions) return;
			if(!data.message && (!data.link || (data.link && !data.name))) return;
			if(data.application && data.application.namespace && data.application.canvas_name) return;
			
            if(data.id) this.entry.push ( new FacebookFeedEntry(data) );
        }.bind(this));
    }
});

var FeedEntry = new Class({
    story: null,
	source: null,
	html: null,
	
	initialize: function(feed_entry_data){
		this.story = this.parse_feed_entry(feed_entry_data);
    },
	
	create_index: function(timestamp){
		return new Date().parse(timestamp).format("%m%d%H%M%S");
	},
	
	get_publication_date: function(timestamp){
		return new Date().parse(timestamp).diff(new Date(), 'day');
	},
	
	get_domain_name: function(url){
        var full_domain_name = new URI(url).get('host');
        var domain_name = full_domain_name.replace(/www\./i, '');
        return domain_name;
    },
	
	highlight_links: function(message){
		return message.replace(/http(s)?:\/\/([^ ]*)/i,'<span class="url">$2</span>');
	},
	
	get_story_element: function (){
		return new Element('a',{
            'target' : '_blank',
            'class': 'feed_entry '+this.source,
            'href' : this.story.url
        });
	},
	
	get_message_element: function(){
		return new Element('span', {
			'class' : 'message',
			'html' : this.highlight_special_words(this.story.message)
		});
	},
	
	get_mentions_element: function(){
		if(!this.story.mentions || this.story.mentions == '') return;

		var names = ' @ '+this.get_mentions_names(this.story.mentions);
		names = names.replace(/, $/i, '');
		
		return new Element('span',{
			'class' : 'to',
			'text' : names
		});
	},
	
	get_publisher_element: function(){
		return new Element('p', {
			'class' : 'user'
		}).adopt(
			new Element('span',{
				'class' : 'from',
				'text' : this.story.from
			})
		);
	},
	
    show: function(){		
		var story = this.get_story_html();
		if(this.story.days_ago == 0) {
			$('today').setStyle('display', 'block');
			story.inject($('yesterday'), 'before');
		} else if (this.story.days_ago == 1) {
			$('yesterday').setStyle('display', 'block');
			story.inject($('later'), 'before');
		} else {
			$('later').setStyle('display', 'block');
			story.inject($('wall'));
		}
    }
});

var TwitterFeedEntry = new Class({
	Extends: FeedEntry,
	source: 'twitter',
	
	parse_feed_entry: function(tweet_data){
		return {
			index : this.create_index(tweet_data.created_at),
			days_ago : this.get_publication_date(tweet_data.created_at),
			url : 'https://twitter.com/'+tweet_data.user.screen_name+'/status/'+tweet_data.id_str,
			from : tweet_data.user.name,
			mentions : tweet_data.entities.user_mentions,
			hashtags : tweet_data.entities.hashtags,
			link : tweet_data.entities.urls,
			message : tweet_data.text
		}
	},
	
	get_mentions_names: function(){
		var names = '';
		Array.each(this.story.mentions, function( user ){
			names = names + user.name + ', ';
		});
		
		return names;
	},
	
	highlight_special_words: function(message){
		message = this.highlight_usernames(message);
		message = this.highlight_hashtags(message);
		message = this.expand_links(message);
		message = this.highlight_links(message);
		return message;
	},
	
	highlight_usernames: function(message){
		return message.replace(/[@][\w_]*/ig, '<span class="username">$&</span>');
	},
	
	highlight_hashtags: function(message){
		for( i in this.story.hashtags ){
			message = message.replace('#'+this.story.hashtags[i].text, '<hashtag>'+this.story.hashtags[i].text+'</hashtag>');
		}
		
		message = message.replace(/<hashtag>([^<]*)<\/hashtag>/ig, '<span class="hashtag">#$1</span>');
		
		return message;
	},
	
	expand_links: function(message){
		if(!this.story.link) return;
		
		Array.each( this.story.link, function(url){
			if(!url.display_url) return;
			message = message.replace(url.url,'<span class="url">'+url.display_url+'</span>');
		});
		
		return message;
	},
	
	get_story_html: function(){
		if(this.html) return this.html;
		
		this.html = this.get_story_element().adopt(
			this.get_publisher_element().adopt(
				this.get_mentions_element()
			),
			this.get_message_element()
		);
		
		return this.html;
	}
	
});

var FacebookFeedEntry = new Class({
    Extends: FeedEntry,
	source : 'facebook',
	
	parse_feed_entry: function(status_update_data){
		var data = {
			index : this.create_index(status_update_data.created_time),
			days_ago : this.get_publication_date(status_update_data.created_time),
			from : status_update_data.from.name,
			url : status_update_data.actions[0].link,
			message : '',
			link : ''
		};
		
		if(status_update_data.to) data['mentions'] = status_update_data.to.data;
		if(status_update_data.message) data['message'] = status_update_data.message;
		
		if(status_update_data.link && status_update_data.name)
			data['link'] = {
				'url' : status_update_data.link,
				'title' :status_update_data.name
			};
		else if(status_update_data.link && status_update_data.caption)
			data['link'] = {
				'url' : status_update_data.link,
				'title' :status_update_data.caption
			};
		else if(status_update_data.link && status_update_data.type == 'photo')
			data['link'] = {
				'url' : status_update_data.link,
				'title' : 'Photo'
			};
		
		return data;
	},
	
	get_mentions_names: function(){
		var names = '';
		Array.each(this.story.mentions, function( user ){
			names = names + user.name + ', ';
		});
		
		return names;
	},
    
	get_link_element: function(){
		if(!this.story.link) return;
		
		return new Element('p', {
			'class' : 'link'
		}).adopt(
			new Element('span', {
				'class' : 'title',
				'text' : this.story.link.title
			}),
			new Element('span', {
				'class' : 'url',
				'text' : ' - ' + this.get_domain_name(this.story.link.url)
			})
		);
	},
	
	highlight_special_words: function(message){
		message = this.highlight_usernames(message);
		message = this.highlight_links(message);
		return message;
	},
	
	highlight_usernames: function(message){
		if(!this.story.mentions) return message;

		Array.each(this.story.mentions, function( user ){
			message = message.replace(user.name, '<span class="username">$&</span>');
		});
		
		return message;
	},
		
    get_story_html: function(){
		if(this.html) return this.html;
		
		this.html = this.get_story_element().adopt(
			this.get_publisher_element().adopt(
				this.get_mentions_element()
			),
			this.get_message_element(),
			this.get_link_element()
		);
		
		return this.html;
    }
});
