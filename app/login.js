window.addEvent('domready', function(){
	load_facebook_api();
});

function load_facebook_api(){
	new Element('div', {id : 'fb-root'}).inject($('body'), 'top');
	FB.init({ appId: '285812584763963', cookie:true, oauth: true});
	
	if(Cookie.read('use_facebook')){
		FB.getLoginStatus(function(response){
			react_to_facebook_login_status(response);
		}, true);
	}

	$$('#facebook .add').addEvent('click', function(){
		FB.login(function(response){
			react_to_facebook_login_status(response);
		}, {scope: 'user_status, read_stream'});
	});
	
	$$('#facebook .remove').addEvent('click', function(){
			Cookie.dispose('use_facebook');
			$$('#facebook').set('class', 'disconnected');
	});
}

function react_to_facebook_login_status(response){
	if(response.status == 'connected') {
		FB.api('/fql', {'q' : 'SELECT read_stream, user_status FROM permissions WHERE uid = me()' }, function(response){
			if(response.data[0].read_stream && response.data[0].user_status) {
				Cookie.write('use_facebook', true, {path : "/", duration: 365});
				$$('#facebook').set('class', 'connected');
			} else {
				Cookie.dispose('use_facebook');
				$$('#facebook').set('class', 'disconnected');
			}
		});
	} else
			$$('#facebook').set('class', 'disconnected');
}