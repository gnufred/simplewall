<?php
	error_reporting(E_ALL);
	ini_set('error_reporting', E_ALL);

	include ("twitter/api.php");
	
	if(!$_COOKIE['use_twitter']){
		try {
			$twitter_login_url = $twitter->getAuthenticateUrl(null,
				array(
				'oauth_callback' => 
				"http://".$_SERVER['SERVER_NAME'].str_replace("login.php", 'twitter/authorize.php', $_SERVER['REQUEST_URI'])
				)
			);
		} catch (Exception $e) {}
	}
	
?><!DOCTYPE HTML>
<html>
	<head>
		<title>Simple Wall - The simple social network feed</title>
		
        <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
        
		<meta name="viewport" content="width=768px, minimum-scale=1.0, maximum-scale=1.0" />
		<meta name="viewport" content="width=320px, minimum-scale=1.0, maximum-scale=1.0" />
		<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
		<link href="images/simplewall-16px.png" rel="shortcut icon">
		<link href="css/login.css" rel="stylesheet">
        
		<script src="http://www.google-analytics.com/ga.js"></script>
		<script src="http://connect.facebook.net/en_US/all.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/mootools/1.4.1/mootools.js"></script>
		<script src="lib/mootools-more.js"></script>
		
		<script src="app/login.js"></script>
		<script src="app/analytics.js"></script>

	</head>
	<body id="body">
		<p>Add your favorite social networks</p>
		<div id="facebook" class="<?=($_COOKIE['use_facebook']?'connected':'disconnected')?>">
			<h2>Facebook</h2>
			(<a href="#" class="add">Add</a><a href="#" class="remove">Remove</a>)
		</div>
		<div id="twitter" class="<?=($_COOKIE['use_twitter']?'connected':'disconnected')?>">
			<h2>Twitter</h2>
			(<a href="<?=(!$_COOKIE['use_twitter']?$twitter_login_url:'')?>" class="add">Add</a><a href="twitter/logout.php" class="remove">Remove</a>)
		</div>
		<div id="simplewall">
			<h2>SimpleWall</h2><br>
			<a href="wall.html">View your wall</a>
		</div>
		<p><small>Created by <a href="http://twitter.com/akeif" target="_blank">@Akeif</a></small></p>
	</body>
</html>