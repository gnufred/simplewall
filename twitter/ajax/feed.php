<?php
include("../api.php");

$options = array(
	'include_rts' => false,
	'include_entities' => true,
	'exclude_replies' => true,
	'count' => 35
);

if(isset($_GET['max_id'])) $options['max_id'] = $_GET['max_id'];
if(isset($_GET['since_id'])) $options['since_id'] = $_GET['since_id'];

try {
	$feed = $twitter->get(
		'/statuses/home_timeline.json',
		$options
	);

	$json_feed = json_encode($feed);
	echo($json_feed);
} catch (Exception $e){
	header("HTTP/1.0 401 Unauthorized");
}