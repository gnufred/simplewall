<?php
include 'epi/EpiCurl.php';
include 'epi/EpiOAuth.php';
include 'epi/EpiTwitter.php';
include 'epi/crypt.php';

$app_key = '';
$app_secret = '';

if($_COOKIE['use_twitter'])
	$twitter = new EpiTwitter($app_key, $app_secret, decrypt($_COOKIE['twttr_tkn']), decrypt($_COOKIE['twttr_tkn_secret']));
else
	$twitter = new EpiTwitter($app_key, $app_secret);