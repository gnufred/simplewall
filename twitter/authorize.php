<?php
include("api.php");

if($_GET['oauth_token']){
	$twitter->setToken($_GET['oauth_token']);  
	$token = $twitter->getAccessToken();  
	$twitter->setToken($token->oauth_token, $token->oauth_token_secret);
	setcookie('use_twitter', 1, time()+3600*24*365, '/');
	setcookie('twttr_tkn', encrypt($token->oauth_token), time()+3600*24*365, '/');
	setcookie('twttr_tkn_secret', encrypt($token->oauth_token_secret), time()+3600*24*365, '/');
}

header( 'Location: ../login.php' );